//Activity:
/* 
    1.) Create a new set of pokemon battle. (Same as our discussion)
    -Solve the health of the pokemon that when tackled is invoked,
    current value of the target's health should decrease continuously
    as many times the tackle is involved.
    (target.health - this.attack)

    2.) If health is below 5, invoke faint function

*/
function Pokemon(name, level) {

    //properties
    this.name = name;
    this.level = level;
    this.health = 10 * level;
    this.attack = level;

    //methods
    this.tackle = function (target) {

        let x = target.health - this.attack;

        console.log(this.name + " tackled " + target.name)
        console.log(target.name + "'s health is now reduced to " + (x));

        target.health = x;

        if (target.health <= 5) {
            console.log(this.name + " fainted.")
        }

    }

}; this.fainted = function () {

    console.log(this.name + "fainted.")

}


let pikachu = new Pokemon("Pikachu", 16)
let charizard = new Pokemon("Charizard", 8)


